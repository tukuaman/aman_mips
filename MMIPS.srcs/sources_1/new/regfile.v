module regfile(en,CLK,rst,WE,RA1,RA2,WA,WD,RD1,RD2,test_in,test_out);
input CLK,WE,rst,en;
input[2:0] RA1,RA2,WA;
input[7:0] WD;
output[7:0] RD1,RD2;

input[2:0] test_in;
output[7:0] test_out;

reg [7:0] filemem [0:7];
//asynchronous read from 2 locs
assign RD1 = filemem[RA1];
assign RD2 = filemem[RA2];

assign test_out = filemem[test_in];

//synchronous write on WE
always@(rst or CLK)
//always@(posedge rst or posedge CLK)
begin
//    if(en)
//    begin
    if(rst)
    begin
        filemem[0] <= 'd0;
        filemem[1] <= 'd0;
        filemem[2] <= 'd0;
        filemem[3] <= 'd0;
        filemem[4] <= 'd0;
        filemem[5] <= 'd0;
        filemem[6] <= 'd0;
        filemem[7] <= 'd0;
    end
    else if(CLK)
    begin
        if(WE)
        begin
            if(WA != 'd0)
                filemem[WA] <= WD;
        //$display("write on r%d with val %d",WA,WD);
        //end        
        end
    end
//    end
end 
endmodule

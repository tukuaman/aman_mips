module instmem(rst,A,RD,en);
input[7:0] A;
input rst;
output reg[15:0] RD;
output reg en;

reg[7:0] RAM [0:255];

//assign en = rst ? 'b1: ({RAM[A],RAM[A+1]} ? 'd1 : 'd0) ;


always @(A)
begin
    RD <= {RAM[A],RAM[A+1]};
end
// this is where the  assembly code is written into

always@(posedge rst)
begin
    en <= 'b1;
//	RAM[0] <= 8'h40;
//	RAM[1] <= 8'h41;
//	RAM[2] <= 8'h40;
//	RAM[3] <= 8'h81;
//	RAM[4] <= 8'h43;
//	RAM[5] <= 8'h82;
//	RAM[6] <= 8'h41;
//	RAM[7] <= 8'h41;
//	RAM[8] <= 8'h41;
//	RAM[9] <= 8'h00;
//	RAM[10] <= 8'h08;
//	RAM[11] <= 8'h55;
//	RAM[12] <= 8'h0D;
//	RAM[13] <= 8'h8D;
//	RAM[14] <= 8'h00;
//	RAM[15] <= 8'h00;

	RAM[0] <= 8'h40;
	RAM[1] <= 8'h41;
	RAM[2] <= 8'h40;
	RAM[3] <= 8'h81;
	RAM[4] <= 8'h43;
	RAM[5] <= 8'h82;
	RAM[6] <= 8'h41;
	RAM[7] <= 8'h41;
	RAM[8] <= 8'h41;
	RAM[9] <= 8'h00;
	RAM[10] <= 8'hF0;
	RAM[11] <= 8'h60;
	RAM[12] <= 8'h41;
	RAM[13] <= 8'h21;
	RAM[14] <= 8'hF0;
	RAM[15] <= 8'hA0;
	RAM[16] <= 8'h0B;
	RAM[17] <= 8'h40;
	RAM[18] <= 8'h41;
	RAM[19] <= 8'h69;
	RAM[20] <= 8'h06;
	RAM[21] <= 8'h50;
	RAM[22] <= 8'h41;
	RAM[23] <= 8'h21;
	RAM[24] <= 8'h41;
	RAM[25] <= 8'h69;
	RAM[26] <= 8'hF0;
	RAM[27] <= 8'hE0;
	RAM[28] <= 8'h40;
	RAM[29] <= 8'h50;
	RAM[30] <= 8'h40;
	RAM[31] <= 8'h98;
	RAM[32] <= 8'h81;
	RAM[33] <= 8'h72;
	RAM[34] <= 8'h20;
	RAM[35] <= 8'h0A;
	RAM[36] <= 8'h00;
	RAM[37] <= 8'h00;
end

endmodule
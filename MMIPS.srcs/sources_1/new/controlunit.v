module controlunit(rst,en,op,funct,
                   jump,
                   MemtoReg,
                   MemWrite,
                   Branch,
                   ALUCtrl,
                   ALUSrc,
                   RegDst,
                   RegWrite, temp);
input en,rst;
input[3:0] op;
input[2:0] funct;
output reg jump,MemtoReg,MemWrite,Branch,ALUSrc,RegDst,RegWrite;
output reg[2:0] ALUCtrl;
output reg [3:0] temp;


always @(op or funct or rst)
begin
    temp <= op;
    if(rst)
    begin
        jump <= 'd0;
        MemtoReg <= 'd0;
        MemWrite <= 'd0;
        Branch <= 'd0;
        ALUSrc <= 'd0;
        RegDst <= 'd0;
        RegWrite <= 'd0;
        ALUCtrl <= 'd0;
    end
    else if(en)
    begin
    case(op)
    //R-type instructions
    'b0000:
        begin
            case(funct)
            'b000: //ADD
            begin
                jump <= 'b0;
                MemtoReg <= 'b0;
                MemWrite <= 'b0;
                Branch <= 'b0;
                ALUSrc <= 'b0;
                RegDst <= 'b1;
                RegWrite <= 'b1;
                ALUCtrl <= 'd0;
            end
            'b010: //SUB
            begin
                jump <= 'b0;
                MemtoReg <= 'b0;
                MemWrite <= 'b0;
                Branch <= 'b0;
                ALUSrc <= 'b0;
                RegDst <= 'b1;
                RegWrite <= 'b1;
                ALUCtrl <= 'd2;
            end
            'b100: //AND
            begin
                jump <= 'b0;
                MemtoReg <= 'b0;
                MemWrite <= 'b0;
                 Branch <= 'b0;
                ALUSrc <= 'b0;
                RegDst <= 'b1;
                RegWrite <= 'b1;
                ALUCtrl <= 'd4;               
            end
            'b101: //OR
             begin
                jump <= 'b0;
                MemtoReg <= 'b0;
                MemWrite <= 'b0;
                Branch <= 'b0;
                ALUSrc <= 'b0;
                RegDst <= 'b1;
                RegWrite <= 'b1;
                ALUCtrl <= 'd5;               
            end
            endcase           
        end
        'b0100: //ADDI
        begin
            jump <= 'b0;
            MemtoReg <= 'b0;
            MemWrite <= 'b0;
            Branch <= 'b0;
            ALUSrc <= 'b1;
            RegDst <= 'b0;
            RegWrite <= 'b1;
            ALUCtrl <= 'd0;
        end
        'b1011: //LW
        begin
            jump <= 'b0;
            MemtoReg <= 'b1;
            MemWrite <= 'b0;
            Branch <= 'b0;
            ALUSrc <= 'b1;
            RegDst <= 'b0;
            RegWrite <= 'b1;
            ALUCtrl <= 'd0;
        end
        'b1111: //SW
        begin
            jump <= 'b0;
            // MemtoReg dont care
            MemWrite <= 'b1;
            Branch <= 'b0;
            ALUSrc <= 'b1;
            //RegDst <= 'b1; dontcare
            RegWrite <= 'b0;
            ALUCtrl <= 'd0;
        end
        'b1000: //BEQ
        begin
            jump <= 'b0;
            // MemtoReg dont care
            MemWrite <= 'b0;
            Branch <= 'b1;
            ALUSrc <= 'b0;
            //RegDst <= 'b1; dontcare
            RegWrite <= 'b0;
            ALUCtrl <= 'd2;
        end
        'b0010: //J
        begin
           jump <= 'b1;
           //others are dont care 
        end
        endcase
end //end if
end
endmodule
//module topmodule(test_out);
module topmodule(test_in,test_out,test_sel);
//input rst,clk;
input test_sel;
input[2:0] test_in;
output[7:0] test_out;

wire rst,clk;
//wire[2:0] test_in;

wire jump,MemtoReg,MemWrite,Branch,ALUSrc,RegDst,RegWrite;
wire [2:0] ALUCtrl;
wire en;
wire[35:0] control;
wire signed [7:0] rd1,rd2,Result,PCBranch;
wire zero,PCSrc;
wire [15:0] instr;
wire signed [7:0] SrcB,Signlmm,ALUResult,ReadData;
wire [2:0] WriteReg;
wire[7:0] PC,PC11,PC10,mux1,mux2,PC21;

wire[7:0] reg_file_test;
wire[7:0] ctrl_signl_test;
wire [3:0] wtemp;
reg [2:0] WA_prev;
reg RegWrite_prev;
wire wRegWrite;
reg [7:0] Result_prev;

icon icon1(.CONTROL0(control));
vio vio1 (.CONTROL(control), .ASYNC_OUT({rst, clk}), .ASYNC_IN({RegWrite_prev,instr,PC,rd1,SrcB,ALUResult, WA_prev}));
//vio vio1 (.CONTROL(control), .ASYNC_OUT({rst, clk, test_sel, test_in}),.ASYNC_IN({ALUCtrl[0],jump,MemtoReg,MemWrite,Branch,ALUSrc,RegDst,RegWrite,en,instr,PC, reg_file_test}));


assign ctr_signl_test = {ALUCtrl[0],jump,MemtoReg,MemWrite,Branch,ALUSrc,RegDst,RegWrite};
instmem i1(rst,PC,instr,en);
controlunit c1(rst,en,instr[15:12],instr[2:0],jump,MemtoReg,MemWrite,Branch,ALUCtrl,ALUSrc,RegDst,RegWrite,wtemp);
regfile r1(en,clk,rst,wRegWrite,instr[5:3],instr[8:6],WriteReg,Result,rd1,rd2,test_in,reg_file_test);
alu a1(rst,en,ALUCtrl,rd1,SrcB,zero,ALUResult);
datamem d1(en,clk,ALUResult,rd2,MemWrite,ReadData);
and x1(PCSrc,zero,Branch);
pc p1(en,clk,rst,mux2,PC);
//assign WriteReg = (RegDst) ? instr[11:9] : instr[8:6];
assign WriteReg = WA_prev;
assign wRegWrite = RegWrite_prev;
//getting the correct immediate value
assign Signlmm = {{2{instr[11]}},{instr[11:9],instr[2:0]}};
assign SrcB = (ALUSrc) ? Signlmm : rd2;
//assign Result = (MemtoReg)? ReadData : ALUResult;
assign Result = Result_prev;
// PC + 2*imm
assign PC11 = ((Signlmm << 1) + PC);
assign PC10 = PC + 'd2;
assign mux1 = (PCSrc) ? PCBranch : PC10;
assign PC21 = (instr[6:0]) << 1;
assign mux2 = (jump) ? PC21 : mux1;

assign test_out = test_sel ? {ALUCtrl[0],jump,MemtoReg,MemWrite,Branch,ALUSrc,RegDst,RegWrite} : reg_file_test;

always @ (clk)
begin
    if (!clk)
    begin
    WA_prev <= (RegDst) ? instr[11:9] : instr[8:6];
    RegWrite_prev <= RegWrite;
    Result_prev <= (MemtoReg)? ReadData : ALUResult;
    end
end
//initial
//$display("jump,MemtoReg,MemWrite,Branch,ALUSrc,RegDst,RegWrite,ALUCtrl");

//always @(jump,MemtoReg,MemWrite,Branch,ALUSrc,RegDst,RegWrite,ALUCtrl)
//$display("%b %b %b %b %b %b %b %d",jump,MemtoReg,MemWrite,Branch,ALUSrc,RegDst,RegWrite,ALUCtrl);

//always @(Signlmm)
//begin
//$display("imm = %d instr[11:9] = %b, instr[2:0] = %b, instr = %h",Signlmm,instr[11:9], instr[2:0],instr);
//end
endmodule
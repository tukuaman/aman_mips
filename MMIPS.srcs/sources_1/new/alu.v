module alu(rst,en,ALUCtrl,A,B,zero,result);
input[2:0]  ALUCtrl;
input en,rst;
input signed[7:0] A,B;
output zero;
output reg signed [7:0] result;

//zero is true if alu output is zero
assign zero = (rst) ? 'd0 : ((result == 'd0) ? 'b1 : 'b0); 

always@(ALUCtrl,A,B,rst) //re-evaluate if these change
begin
    if(rst)
    begin
        //zero <= 'd0;
        result <= 'd0;
    end
    else if(en)
    begin
   case(ALUCtrl)
   'd1: result <= (A == B); //BEQ *can be replaced by SUB and zero check pass thru AND*
   'd0: result <= A + B; //ADD
   'd2: result <= A - B; //SUB
   'd4: result <= A & B; //AND
   'd5: result <= A | B; //OR
   default: result <= 'd0;
   endcase
   end
end
endmodule
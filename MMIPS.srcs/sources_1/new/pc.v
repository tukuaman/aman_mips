module pc(en,CLK,rst,D,Q);
input CLK,rst,en;
input[7:0] D;
output reg[7:0] Q;


//always @(posedge CLK)
//    Q <= D;

always@(posedge rst or posedge CLK)
begin
//    if(en)
//    begin
//    if(rst)
//        Q <= 'd0;
//    else if(CLK == 'd1)
//        Q <= D;
//    end

    if(rst)
        Q <= 'd0 & {8{en}};
    else
        Q <= D & {8{en}};
end

endmodule
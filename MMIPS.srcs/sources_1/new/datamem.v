module datamem(en,CLK,A,WD,WE,RD);
//ports 
input CLK,WE,en;
input [7:0] A,WD;
output reg [7:0] RD;

//content declaration
reg [7:0] Dmem[0:255];

always @(posedge CLK)
begin
    if(en)
    begin
    case(WE)
    'b0: //memread
        begin
            RD <= Dmem[A];
        end
    'b1: //memwrite
        begin
            Dmem[A]<=WD;
            //$display("num: %d on addr: %d",WD,A);
        end
    endcase
    end
end
endmodule
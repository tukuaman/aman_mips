module tb;
reg clk,rst;
wire jump,MemtoReg,MemWrite,Branch,ALUSrc,RegDst,RegWrite;
wire [2:0] ALUCtrl;

topmodule T(rst,clk,jump,MemtoReg,MemWrite,Branch,ALUSrc,RegDst,RegWrite,ALUCtrl);

initial
begin
clk = 'b0;
rst = 'b1;
#2 rst = 'b0;
end

always
begin
    #5 clk = ~clk;
end

initial
begin
#900 $finish;
end
endmodule
# README #

### What is this repository for? ###

* This is a Xilinx ISE PlanAhead project for simulation and implementation of 8-bit MIPS with 9 instructions and 3 instruction formats on Spartan 3E FPGA board.
* Final version

### Who do I talk to? ###

* Aman Goel (Email - ee11b087@ee.iitm.ac.in, Webpage - http://www.ee.iitm.ac.in/~ee11b087/index.html)
--------------------------------------------------------------------------------
-- rom.vhd - fibo.mips
-- Created on 28-04-2015 by MiniMIPS Sim 1.0
--------------------------------------------------------------------------------
-- addi $r1, $r0, 1               # num 1
-- addi $r2, $r0, 1               # num 2
-- addi $r6, $r0, 10              # numbers reqd
-- addi $r5, $r0, 1               # counter
-- addi $r4, $r0, 0               # dmem address
-- sw   $r1, 0($r4)               # 
-- addi $r4, $r4, 1               # incrising dmem address
-- sw   $r2, 0($r4)               # incrising dmem address
-- add  $r5, $r5, $r0             # 
-- addi $r5, $r5, 1               # 
-- add  $r3, $r1, $r2             # 
-- addi $r4, $r4, 1               # 
-- addi $r5, $r5, 1               # 
-- sw   $r3, 0($r4)               # 
-- addi $r1, $r2, 0               # 
-- addi $r2, $r3, 0               # 
-- beq  $r5, $r6, 1               # 
-- j    5                         # 
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity I_MEM is
    Port ( Addr : in  STD_LOGIC_VECTOR (7 downto 0);
           Instr : out  STD_LOGIC_VECTOR (15 downto 0));
end I_MEM;

architecture Behavioral of I_MEM is

type rom is array (0 to 255) of std_logic_vector (7 downto 0);

constant code : rom :=(x"40",x"41",  -- addi $r1, $r0, 1
                       x"40",x"81",  -- addi $r2, $r0, 1
                       x"41",x"8A",  -- addi $r6, $r0, 10
                       x"41",x"41",  -- addi $r5, $r0, 1
                       x"41",x"00",  -- addi $r4, $r0, 0
                       x"F8",x"40",  -- sw   $r1, 0($r4)
                       x"49",x"01",  -- addi $r4, $r4, 1	
                       x"F8",x"80",  -- sw   $r2, 0($r4)
                       x"0A",x"28",  -- add  $r5, $r5, $r0
                       x"4B",x"41",  -- addi $r5, $r5, 1
                       x"02",x"98",  -- add  $r3, $r1, $r2
                       x"49",x"01",  -- addi $r4, $r4, 1
                       x"4B",x"41",  -- addi $r5, $r5, 1
                       x"F8",x"C0",  -- sw   $r3, 0($r4)
                       x"44",x"40",  -- addi $r1, $r2, 0
                       x"46",x"80",  -- addi $r2, $r3, 0
                       x"8D",x"41",  -- beq  $r5, $r6, 1
                       x"20",x"05",  -- j    5
					   OTHERS => x"00");  
begin
    Instr(15 downto 8) <= code(conv_integer(Addr));
    Instr(7 downto 0) <= code(conv_integer(Addr)+1);
end Behavioral;


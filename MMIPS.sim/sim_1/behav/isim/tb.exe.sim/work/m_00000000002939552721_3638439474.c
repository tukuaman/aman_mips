/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/dheeraj/MMIPS/MMIPS.srcs/sources_1/new/instmem.v";
static unsigned int ng1[] = {1U, 0U};
static int ng2[] = {1, 0};
static unsigned int ng3[] = {0U, 0U};
static const char *ng4 = "IMEM.dat";



static void Cont_9_0(char *t0)
{
    char t3[8];
    char t4[8];
    char t21[8];
    char t22[8];
    char t23[8];
    char t27[8];
    char t36[8];
    char t40[8];
    char *t1;
    char *t2;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t24;
    char *t25;
    char *t26;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t37;
    char *t38;
    char *t39;
    char *t41;
    char *t42;
    char *t43;
    char *t44;
    char *t45;
    char *t46;
    char *t47;
    char *t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    char *t54;
    char *t55;
    unsigned int t56;
    unsigned int t57;
    unsigned int t58;
    char *t59;
    unsigned int t60;
    unsigned int t61;
    unsigned int t62;
    unsigned int t63;
    char *t64;
    char *t65;
    char *t66;
    char *t67;
    char *t68;
    char *t69;
    unsigned int t70;
    unsigned int t71;
    char *t72;
    unsigned int t73;
    unsigned int t74;
    char *t75;
    unsigned int t76;
    unsigned int t77;
    char *t78;

LAB0:    t1 = (t0 + 1616U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(9, ng0);
    t2 = (t0 + 692U);
    t5 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t5 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 & 1U);
    if (t10 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t14 = *((unsigned int *)t12);
    t15 = (t13 || t14);
    if (t15 > 0)
        goto LAB8;

LAB9:    t17 = *((unsigned int *)t4);
    t18 = (~(t17));
    t19 = *((unsigned int *)t12);
    t20 = (t18 || t19);
    if (t20 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t12) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t21, 8);

LAB16:    t65 = (t0 + 2120);
    t66 = (t65 + 32U);
    t67 = *((char **)t66);
    t68 = (t67 + 32U);
    t69 = *((char **)t68);
    memset(t69, 0, 8);
    t70 = 1U;
    t71 = t70;
    t72 = (t3 + 4);
    t73 = *((unsigned int *)t3);
    t70 = (t70 & t73);
    t74 = *((unsigned int *)t72);
    t71 = (t71 & t74);
    t75 = (t69 + 4);
    t76 = *((unsigned int *)t69);
    *((unsigned int *)t69) = (t76 | t70);
    t77 = *((unsigned int *)t75);
    *((unsigned int *)t75) = (t77 | t71);
    xsi_driver_vfirst_trans(t65, 0, 0);
    t78 = (t0 + 2068);
    *((int *)t78) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t11 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB7;

LAB8:    t16 = ((char*)((ng1)));
    goto LAB9;

LAB10:    t24 = (t0 + 1104);
    t25 = (t24 + 36U);
    t26 = *((char **)t25);
    t28 = (t0 + 1104);
    t29 = (t28 + 44U);
    t30 = *((char **)t29);
    t31 = (t0 + 1104);
    t32 = (t31 + 40U);
    t33 = *((char **)t32);
    t34 = (t0 + 600U);
    t35 = *((char **)t34);
    t34 = ((char*)((ng2)));
    memset(t36, 0, 8);
    xsi_vlog_unsigned_add(t36, 32, t35, 8, t34, 32);
    xsi_vlog_generic_get_array_select_value(t27, 16, t26, t30, t33, 2, 1, t36, 32, 2);
    t37 = (t0 + 1104);
    t38 = (t37 + 36U);
    t39 = *((char **)t38);
    t41 = (t0 + 1104);
    t42 = (t41 + 44U);
    t43 = *((char **)t42);
    t44 = (t0 + 1104);
    t45 = (t44 + 40U);
    t46 = *((char **)t45);
    t47 = (t0 + 600U);
    t48 = *((char **)t47);
    xsi_vlog_generic_get_array_select_value(t40, 16, t39, t43, t46, 2, 1, t48, 8, 2);
    xsi_vlogtype_concat(t23, 32, 32, 2U, t40, 16, t27, 16);
    memset(t22, 0, 8);
    t47 = (t23 + 4);
    t49 = *((unsigned int *)t47);
    t50 = (~(t49));
    t51 = *((unsigned int *)t23);
    t52 = (t51 & t50);
    t53 = (t52 & 4294967295U);
    if (t53 != 0)
        goto LAB17;

LAB18:    if (*((unsigned int *)t47) != 0)
        goto LAB19;

LAB20:    t55 = (t22 + 4);
    t56 = *((unsigned int *)t22);
    t57 = *((unsigned int *)t55);
    t58 = (t56 || t57);
    if (t58 > 0)
        goto LAB21;

LAB22:    t60 = *((unsigned int *)t22);
    t61 = (~(t60));
    t62 = *((unsigned int *)t55);
    t63 = (t61 || t62);
    if (t63 > 0)
        goto LAB23;

LAB24:    if (*((unsigned int *)t55) > 0)
        goto LAB25;

LAB26:    if (*((unsigned int *)t22) > 0)
        goto LAB27;

LAB28:    memcpy(t21, t64, 8);

LAB29:    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 32, t16, 32, t21, 32);
    goto LAB16;

LAB14:    memcpy(t3, t16, 8);
    goto LAB16;

LAB17:    *((unsigned int *)t22) = 1;
    goto LAB20;

LAB19:    t54 = (t22 + 4);
    *((unsigned int *)t22) = 1;
    *((unsigned int *)t54) = 1;
    goto LAB20;

LAB21:    t59 = ((char*)((ng1)));
    goto LAB22;

LAB23:    t64 = ((char*)((ng3)));
    goto LAB24;

LAB25:    xsi_vlog_unsigned_bit_combine(t21, 32, t59, 32, t64, 32);
    goto LAB29;

LAB27:    memcpy(t21, t59, 8);
    goto LAB29;

}

static void Initial_11_1(char *t0)
{

LAB0:    xsi_set_current_line(12, ng0);

LAB2:    xsi_set_current_line(13, ng0);
    xsi_vlogfile_readmemh(ng4, 0, 0, 0, 0, 0, 0, 0);

LAB1:    return;
}

static void Always_16_2(char *t0)
{
    char t4[8];
    char t8[8];
    char t17[8];
    char t21[8];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    char *t6;
    char *t7;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t18;
    char *t19;
    char *t20;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;

LAB0:    t1 = (t0 + 1888U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(16, ng0);
    t2 = (t0 + 2076);
    *((int *)t2) = 1;
    t3 = (t0 + 1912);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(17, ng0);

LAB5:    xsi_set_current_line(18, ng0);
    t5 = (t0 + 1104);
    t6 = (t5 + 36U);
    t7 = *((char **)t6);
    t9 = (t0 + 1104);
    t10 = (t9 + 44U);
    t11 = *((char **)t10);
    t12 = (t0 + 1104);
    t13 = (t12 + 40U);
    t14 = *((char **)t13);
    t15 = (t0 + 600U);
    t16 = *((char **)t15);
    t15 = ((char*)((ng2)));
    memset(t17, 0, 8);
    xsi_vlog_unsigned_add(t17, 32, t16, 8, t15, 32);
    xsi_vlog_generic_get_array_select_value(t8, 16, t7, t11, t14, 2, 1, t17, 32, 2);
    t18 = (t0 + 1104);
    t19 = (t18 + 36U);
    t20 = *((char **)t19);
    t22 = (t0 + 1104);
    t23 = (t22 + 44U);
    t24 = *((char **)t23);
    t25 = (t0 + 1104);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    t28 = (t0 + 600U);
    t29 = *((char **)t28);
    xsi_vlog_generic_get_array_select_value(t21, 16, t20, t24, t27, 2, 1, t29, 8, 2);
    xsi_vlogtype_concat(t4, 32, 32, 2U, t21, 16, t8, 16);
    t28 = (t0 + 920);
    xsi_vlogvar_wait_assign_value(t28, t4, 0, 0, 16, 0LL);
    goto LAB2;

}


extern void work_m_00000000002939552721_3638439474_init()
{
	static char *pe[] = {(void *)Cont_9_0,(void *)Initial_11_1,(void *)Always_16_2};
	xsi_register_didat("work_m_00000000002939552721_3638439474", "isim/tb.exe.sim/work/m_00000000002939552721_3638439474.didat");
	xsi_register_executes(pe);
}

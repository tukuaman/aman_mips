/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/dheeraj/MMIPS/MMIPS.srcs/sources_1/new/regfile.v";
static unsigned int ng1[] = {0U, 0U};
static int ng2[] = {0, 0};



static void Cont_9_0(char *t0)
{
    char t5[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    unsigned int t18;
    unsigned int t19;
    char *t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    char *t26;

LAB0:    t1 = (t0 + 2168U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(9, ng0);
    t2 = (t0 + 1656);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t6 = (t0 + 1656);
    t7 = (t6 + 44U);
    t8 = *((char **)t7);
    t9 = (t0 + 1656);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    t12 = (t0 + 968U);
    t13 = *((char **)t12);
    xsi_vlog_generic_get_array_select_value(t5, 8, t4, t8, t11, 2, 1, t13, 3, 2);
    t12 = (t0 + 2680);
    t14 = (t12 + 32U);
    t15 = *((char **)t14);
    t16 = (t15 + 32U);
    t17 = *((char **)t16);
    memset(t17, 0, 8);
    t18 = 255U;
    t19 = t18;
    t20 = (t5 + 4);
    t21 = *((unsigned int *)t5);
    t18 = (t18 & t21);
    t22 = *((unsigned int *)t20);
    t19 = (t19 & t22);
    t23 = (t17 + 4);
    t24 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t24 | t18);
    t25 = *((unsigned int *)t23);
    *((unsigned int *)t23) = (t25 | t19);
    xsi_driver_vfirst_trans(t12, 0, 7);
    t26 = (t0 + 2620);
    *((int *)t26) = 1;

LAB1:    return;
}

static void Cont_10_1(char *t0)
{
    char t5[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    unsigned int t18;
    unsigned int t19;
    char *t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    char *t26;

LAB0:    t1 = (t0 + 2304U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(10, ng0);
    t2 = (t0 + 1656);
    t3 = (t2 + 36U);
    t4 = *((char **)t3);
    t6 = (t0 + 1656);
    t7 = (t6 + 44U);
    t8 = *((char **)t7);
    t9 = (t0 + 1656);
    t10 = (t9 + 40U);
    t11 = *((char **)t10);
    t12 = (t0 + 1060U);
    t13 = *((char **)t12);
    xsi_vlog_generic_get_array_select_value(t5, 8, t4, t8, t11, 2, 1, t13, 3, 2);
    t12 = (t0 + 2716);
    t14 = (t12 + 32U);
    t15 = *((char **)t14);
    t16 = (t15 + 32U);
    t17 = *((char **)t16);
    memset(t17, 0, 8);
    t18 = 255U;
    t19 = t18;
    t20 = (t5 + 4);
    t21 = *((unsigned int *)t5);
    t18 = (t18 & t21);
    t22 = *((unsigned int *)t20);
    t19 = (t19 & t22);
    t23 = (t17 + 4);
    t24 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t24 | t18);
    t25 = *((unsigned int *)t23);
    *((unsigned int *)t23) = (t25 | t19);
    xsi_driver_vfirst_trans(t12, 0, 7);
    t26 = (t0 + 2628);
    *((int *)t26) = 1;

LAB1:    return;
}

static void Always_13_2(char *t0)
{
    char t20[8];
    char t21[8];
    char t53[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    char *t19;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;
    unsigned int t30;
    int t31;
    char *t32;
    unsigned int t33;
    int t34;
    int t35;
    unsigned int t36;
    unsigned int t37;
    int t38;
    int t39;
    unsigned int t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    unsigned int t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    char *t54;
    char *t55;
    char *t56;
    unsigned int t57;
    char *t58;
    unsigned int t59;
    unsigned int t60;
    unsigned int t61;

LAB0:    t1 = (t0 + 2440U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(13, ng0);
    t2 = (t0 + 2636);
    *((int *)t2) = 1;
    t3 = (t0 + 2464);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(14, ng0);

LAB5:    xsi_set_current_line(15, ng0);
    t4 = (t0 + 876U);
    t5 = *((char **)t4);
    t4 = (t5 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB6;

LAB7:
LAB8:    goto LAB2;

LAB6:    xsi_set_current_line(16, ng0);

LAB9:    xsi_set_current_line(17, ng0);
    t11 = (t0 + 784U);
    t12 = *((char **)t11);
    t11 = (t12 + 4);
    t13 = *((unsigned int *)t11);
    t14 = (~(t13));
    t15 = *((unsigned int *)t12);
    t16 = (t15 & t14);
    t17 = (t16 != 0);
    if (t17 > 0)
        goto LAB10;

LAB11:    xsi_set_current_line(19, ng0);
    t2 = (t0 + 600U);
    t3 = *((char **)t2);
    t2 = (t3 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t3);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB15;

LAB16:
LAB17:
LAB12:    goto LAB8;

LAB10:    xsi_set_current_line(18, ng0);
    t18 = ((char*)((ng1)));
    t19 = (t0 + 1656);
    t22 = (t0 + 1656);
    t23 = (t22 + 44U);
    t24 = *((char **)t23);
    t25 = (t0 + 1656);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    t28 = ((char*)((ng2)));
    xsi_vlog_generic_convert_array_indices(t20, t21, t24, t27, 2, 1, t28, 32, 1);
    t29 = (t20 + 4);
    t30 = *((unsigned int *)t29);
    t31 = (!(t30));
    t32 = (t21 + 4);
    t33 = *((unsigned int *)t32);
    t34 = (!(t33));
    t35 = (t31 && t34);
    if (t35 == 1)
        goto LAB13;

LAB14:    goto LAB12;

LAB13:    t36 = *((unsigned int *)t20);
    t37 = *((unsigned int *)t21);
    t38 = (t36 - t37);
    t39 = (t38 + 1);
    xsi_vlogvar_wait_assign_value(t19, t18, 0, *((unsigned int *)t21), t39, 0LL);
    goto LAB14;

LAB15:    xsi_set_current_line(20, ng0);

LAB18:    xsi_set_current_line(21, ng0);
    t4 = (t0 + 692U);
    t5 = *((char **)t4);
    t4 = (t5 + 4);
    t13 = *((unsigned int *)t4);
    t14 = (~(t13));
    t15 = *((unsigned int *)t5);
    t16 = (t15 & t14);
    t17 = (t16 != 0);
    if (t17 > 0)
        goto LAB19;

LAB20:
LAB21:    goto LAB17;

LAB19:    xsi_set_current_line(22, ng0);

LAB22:    xsi_set_current_line(23, ng0);
    t11 = (t0 + 1152U);
    t12 = *((char **)t11);
    t11 = ((char*)((ng1)));
    memset(t20, 0, 8);
    t18 = (t12 + 4);
    t19 = (t11 + 4);
    t30 = *((unsigned int *)t12);
    t33 = *((unsigned int *)t11);
    t36 = (t30 ^ t33);
    t37 = *((unsigned int *)t18);
    t40 = *((unsigned int *)t19);
    t41 = (t37 ^ t40);
    t42 = (t36 | t41);
    t43 = *((unsigned int *)t18);
    t44 = *((unsigned int *)t19);
    t45 = (t43 | t44);
    t46 = (~(t45));
    t47 = (t42 & t46);
    if (t47 != 0)
        goto LAB24;

LAB23:    if (t45 != 0)
        goto LAB25;

LAB26:    t23 = (t20 + 4);
    t48 = *((unsigned int *)t23);
    t49 = (~(t48));
    t50 = *((unsigned int *)t20);
    t51 = (t50 & t49);
    t52 = (t51 != 0);
    if (t52 > 0)
        goto LAB27;

LAB28:
LAB29:    goto LAB21;

LAB24:    *((unsigned int *)t20) = 1;
    goto LAB26;

LAB25:    t22 = (t20 + 4);
    *((unsigned int *)t20) = 1;
    *((unsigned int *)t22) = 1;
    goto LAB26;

LAB27:    xsi_set_current_line(24, ng0);
    t24 = (t0 + 1244U);
    t25 = *((char **)t24);
    t24 = (t0 + 1656);
    t26 = (t0 + 1656);
    t27 = (t26 + 44U);
    t28 = *((char **)t27);
    t29 = (t0 + 1656);
    t32 = (t29 + 40U);
    t54 = *((char **)t32);
    t55 = (t0 + 1152U);
    t56 = *((char **)t55);
    xsi_vlog_generic_convert_array_indices(t21, t53, t28, t54, 2, 1, t56, 3, 2);
    t55 = (t21 + 4);
    t57 = *((unsigned int *)t55);
    t31 = (!(t57));
    t58 = (t53 + 4);
    t59 = *((unsigned int *)t58);
    t34 = (!(t59));
    t35 = (t31 && t34);
    if (t35 == 1)
        goto LAB30;

LAB31:    goto LAB29;

LAB30:    t60 = *((unsigned int *)t21);
    t61 = *((unsigned int *)t53);
    t38 = (t60 - t61);
    t39 = (t38 + 1);
    xsi_vlogvar_wait_assign_value(t24, t25, 0, *((unsigned int *)t53), t39, 0LL);
    goto LAB31;

}


extern void work_m_00000000004138458297_1621107508_init()
{
	static char *pe[] = {(void *)Cont_9_0,(void *)Cont_10_1,(void *)Always_13_2};
	xsi_register_didat("work_m_00000000004138458297_1621107508", "isim/tb.exe.sim/work/m_00000000004138458297_1621107508.didat");
	xsi_register_executes(pe);
}

/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/dheeraj/MMIPS/MMIPS.srcs/sources_1/new/datamem.v";
static unsigned int ng1[] = {0U, 0U};
static unsigned int ng2[] = {1U, 0U};
static const char *ng3 = "num: %d";



static void Always_10_0(char *t0)
{
    char t17[8];
    char t26[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    int t13;
    char *t14;
    char *t15;
    char *t16;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    int t27;
    int t28;
    int t29;
    int t30;
    int t31;

LAB0:    t1 = (t0 + 1800U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(10, ng0);
    t2 = (t0 + 1980);
    *((int *)t2) = 1;
    t3 = (t0 + 1824);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(11, ng0);

LAB5:    xsi_set_current_line(12, ng0);
    t4 = (t0 + 784U);
    t5 = *((char **)t4);
    t4 = (t5 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB6;

LAB7:
LAB8:    goto LAB2;

LAB6:    xsi_set_current_line(13, ng0);

LAB9:    xsi_set_current_line(14, ng0);
    t11 = (t0 + 692U);
    t12 = *((char **)t11);

LAB10:    t11 = ((char*)((ng1)));
    t13 = xsi_vlog_unsigned_case_compare(t12, 1, t11, 32);
    if (t13 == 1)
        goto LAB11;

LAB12:    t2 = ((char*)((ng2)));
    t13 = xsi_vlog_unsigned_case_compare(t12, 1, t2, 32);
    if (t13 == 1)
        goto LAB13;

LAB14:
LAB15:    goto LAB8;

LAB11:    xsi_set_current_line(16, ng0);

LAB16:    xsi_set_current_line(17, ng0);
    t14 = (t0 + 1288);
    t15 = (t14 + 36U);
    t16 = *((char **)t15);
    t18 = (t0 + 1288);
    t19 = (t18 + 44U);
    t20 = *((char **)t19);
    t21 = (t0 + 1288);
    t22 = (t21 + 40U);
    t23 = *((char **)t22);
    t24 = (t0 + 876U);
    t25 = *((char **)t24);
    xsi_vlog_generic_get_array_select_value(t17, 8, t16, t20, t23, 2, 1, t25, 8, 2);
    t24 = (t0 + 1196);
    xsi_vlogvar_wait_assign_value(t24, t17, 0, 0, 8, 0LL);
    goto LAB15;

LAB13:    xsi_set_current_line(20, ng0);

LAB17:    xsi_set_current_line(21, ng0);
    t3 = (t0 + 968U);
    t4 = *((char **)t3);
    t3 = (t0 + 1288);
    t5 = (t0 + 1288);
    t11 = (t5 + 44U);
    t14 = *((char **)t11);
    t15 = (t0 + 1288);
    t16 = (t15 + 40U);
    t18 = *((char **)t16);
    t19 = (t0 + 876U);
    t20 = *((char **)t19);
    xsi_vlog_generic_convert_array_indices(t17, t26, t14, t18, 2, 1, t20, 8, 2);
    t19 = (t17 + 4);
    t6 = *((unsigned int *)t19);
    t27 = (!(t6));
    t21 = (t26 + 4);
    t7 = *((unsigned int *)t21);
    t28 = (!(t7));
    t29 = (t27 && t28);
    if (t29 == 1)
        goto LAB18;

LAB19:    xsi_set_current_line(22, ng0);
    t2 = (t0 + 968U);
    t3 = *((char **)t2);
    xsi_vlogfile_write(1, 0, 0, ng3, 2, t0, (char)118, t3, 8);
    goto LAB15;

LAB18:    t8 = *((unsigned int *)t17);
    t9 = *((unsigned int *)t26);
    t30 = (t8 - t9);
    t31 = (t30 + 1);
    xsi_vlogvar_wait_assign_value(t3, t4, 0, *((unsigned int *)t26), t31, 0LL);
    goto LAB19;

}


extern void work_m_00000000002234675779_2646115994_init()
{
	static char *pe[] = {(void *)Always_10_0};
	xsi_register_didat("work_m_00000000002234675779_2646115994", "isim/tb.exe.sim/work/m_00000000002234675779_2646115994.didat");
	xsi_register_executes(pe);
}
